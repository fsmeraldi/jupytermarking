# Python 3 classes for the automated marking of assignments
# in Jupyter notebooks using the unittest framework.
#
# (C) 2019, 2020 Fabrizio Smeraldi <f.smeraldi@qmul.ac.uk>
# see LICENSE.txt for license information and a disclaimer

import unittest # mandatory
import os # as required
from functools import wraps # makes decorator nicer

# number of questions, each with any no. of subquestions
_nquestions=4  
_marks=[0]*_nquestions


class Marker(unittest.TestCase):
    """ Basic infrastructure for reporting. Do not write any test
    methods here - extend this class to mark each (sub)question, see 
    example below. """
    
    @classmethod
    def setUpClass(cls):
        """ Runs once for each class, before all tests """
        print("==================  Marking Q{:d}{:s} =====================".format(
            cls.question, cls.subquestion))
        # unittest allocates a new instance of the test class before
        # running each test. We deal with that using class variables
        cls.marks=0 # marks accumulator 
        cls.report=[] # all feedback messages

    @classmethod
    def tearDownClass(cls):
        """ Runs once for each class, after all tests """
        # reporting: print all error messages
        for msg in cls.report:
            print(msg)
        # add marks to global list
        _marks[cls.question-1]+=cls.marks
        print ("> Total marks for Q{:d}{:s}: {:d} out of {:d}".format(
            cls.question,cls.subquestion,cls.marks,cls.totalmarks))

    @classmethod
    def mark(cls, mark=None, msg=None, err=None):
        """ Add marks, with optional message and error flag for 
        flagging errors without using an exception """
        if mark!=None:
            cls.marks+=mark
            marktxt=" ({} mk)".format(mark)
        else:
            marktxt=""
        if msg!=None:
            cls.report.append("Passed: "+str(msg)+marktxt)
        if err!=None: # we can pass an exception object directly
            error=str(err) if str(err)!="" else str(type(err))
            cls.report.append("Error: "+error+marktxt)



def markExceptions(test_func):
    """ Decorate test functions with this to capture any exceptions
    and report the corresponding error message """
    # lifts name and doc string of test function
    @wraps(test_func)
    def wrapped_test(self):
        try:
            test_func(self)
        except Exception as e:
            self.mark(err=e)
    return wrapped_test

##################### Question 1 ################################


class Q1aMarker(Marker):
    # these values will appear in the report
    question=1 
    subquestion='a'
    totalmarks=8
    # class variables as required
    fname="test123dfe-46823.fas"
    
    @classmethod
    def tearDownClass(cls):
        """ Final clean-up after all the tests are done. If you are 
        overriding this method, remember to call the superclass 
        method """
        if os.path.exists(cls.fname):
            os.remove(cls.fname)
        # don't forget! 
        super().tearDownClass()

    def setUp(self):
        """ Called before each individual test. You can use this to prepare
        for the tests """
        pass

    def tearDown(self):
        """ Called after each individual test. You can use this 
        to clean up after each test """
        pass 
    
    # All methods named test_something will be called by the 
    # unittest framework.
    def test_single_line_read(self):
        """ Up to 1 mark for reading a single name from the file """
        guestlist=['Some Guy']
        self.writeGuestList(guestlist)
        # all exceptions must be handled
        try:
            guests=readGuestList(self.fname)
            self.assertEqual(len(guests), 1, "No data read from file")
            self.assertIn(guestlist[0], guests[0],
                          "Error reading a single name from the file")
            self.mark(1, "single name read")
            self.assertEqual(guestlist[0], guests[0],
                             "You forgot to strip the trailing newline")
            self.mark(1, "newline stripped")
            # report this section passed
            self.mark(msg="Single line read")
        except Exception as e:
            # exception added to the list of errors for reporting
            self.mark(err=e)

    # we can use this decorator to capture any exceptions
    # and report the error message
    @markExceptions
    def test_multiple_line_read(self):
        guestlist=['The First','The Second', 'The Third']
        self.writeGuestList(guestlist)

        guests=readGuestList(self.fname)
        self.assertEqual(len(guests), len(guestlist),
                         "Some guest is missing - forgot to loop?")
        self.mark(1, "guest list length correct")
        # students may have missed the rstrip bit
        guestset=set([x.rstrip() for x in guests])
        # the list returned by the student code might contain the same
        # name, repeated many times
        self.assertEqual(set(guestlist), guestset,
                         "Not all guest names match")
        self.mark(2, "Multiple line guest list correct")


    @markExceptions
    def test_duplicates_caught(self):
        guestlist=['Uncle Roy', 'Uncle Roy', 'Anne', 'Uncle Roy', 'Anne']
        self.writeGuestList(guestlist)
        guests=readGuestList(self.fname)
        self.assertEqual(len(set(guestlist)), len(guests),
                         "Duplicate names are not discarded")
        self.mark(1, "duplicate names discarded")

    @markExceptions
    def test_aunt_celia_removed(self):
        guestlist=['Jack', 'Aunt Celia', 'Jane']
        self.writeGuestList(guestlist)
        guests=readGuestList(self.fname)
        guests=[n.rstrip() for n in guests]
        if not ('Jane' in guests):
            return # not reading all list, caught elsewhere
        self.assertNotIn("Aunt Celia", guests,
                         "You invited Aunt Celia!")
        self.mark(1, "Aunt_Celia_removed")

    @markExceptions
    def test_exception_raised(self):
        guestlist=['Z' * i for i in range(1,14)] # 13 items
        self.writeGuestList(guestlist)
        self.assertRaises(FullHouseException,
                          readGuestList, self.fname)
        self.mark(1 ,"Exception_raised")


    def writeGuestList(self, guestlist):
        """ write a lists of names to the test file """
        with open(self.fname, "wt") as OUTF:
            for name in guestlist:
                OUTF.write(name+"\n")


# add marker classes for the other questions and subquestions here

##############################################################            
                
def markall():
    """ Call this function to run all marking """
    # make sure marks don't double if we run the marking code twice
    _marks[:]=[0]*_nquestions
    # run all tests.
    # main args fix for getting this to work in a notebook, see
    # https://medium.com/@vladbezden/using-python-unittest-in-ipython-or-jupyter-732448724e31
    unittest.main(argv=['first-arg-is-ignored'], exit=False)
    print("=====================================================")
    print("------------------ Marking summary ------------------")
    print("=====================================================")
    
    for i in range(_nquestions):
        print("Question {:d}:\t{:2d} marks". format(i+1, _marks[i]))
    print("-----------------------------------------------------")
    print("Total marks: {:d}".format(sum(_marks)))
