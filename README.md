Python 3 classes for the automated marking of assignments
in Jupyter notebooks using the unittest framework.

Click on this button to launch this repository as a binder:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Ffsmeraldi%2Fjupytermarking%2Fsrc%2Fmaster/master)

(C) 2019, 2020 Fabrizio Smeraldi <f.smeraldi@qmul.ac.uk>
see LICENSE.txt for license information and a disclaimer
